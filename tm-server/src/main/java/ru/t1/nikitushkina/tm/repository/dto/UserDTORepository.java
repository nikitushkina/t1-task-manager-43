package ru.t1.nikitushkina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.dto.IUserDTORepository;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Role;

import javax.persistence.EntityManager;

public class UserDTORepository extends AbstractDTORepository<UserDTO>
        implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        @NotNull final UserDTO user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        @NotNull final UserDTO user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @Nullable final String middleName
    ) {
        @NotNull final UserDTO user = create(login, password);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        if (middleName != null) user.setMiddleName(middleName);
        if (email != null) user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected Class<UserDTO> getClazz() {
        return UserDTO.class;
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

}
