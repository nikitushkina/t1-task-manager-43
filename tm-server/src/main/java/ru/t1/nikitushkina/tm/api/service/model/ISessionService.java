package ru.t1.nikitushkina.tm.api.service.model;

import ru.t1.nikitushkina.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
