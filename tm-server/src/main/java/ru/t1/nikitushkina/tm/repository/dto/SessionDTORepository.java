package ru.t1.nikitushkina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO>
        implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}
