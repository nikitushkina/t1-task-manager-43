package ru.t1.nikitushkina.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.model.IRepository;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeById(@Nullable String id);

}
