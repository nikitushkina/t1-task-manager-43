package ru.t1.nikitushkina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Role;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDTO create(@NotNull String login,
                   @NotNull String password,
                   @Nullable String email);

    @NotNull
    UserDTO create(@NotNull String login,
                   @NotNull String password,
                   @Nullable Role role);

    @NotNull
    UserDTO create(@NotNull String login,
                   @NotNull String password,
                   @Nullable String email,
                   @NotNull String lastname,
                   @NotNull String firstName,
                   @Nullable String middleName);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    Boolean isEmailExists(@NotNull String email);

    Boolean isLoginExists(@NotNull String login);

}
