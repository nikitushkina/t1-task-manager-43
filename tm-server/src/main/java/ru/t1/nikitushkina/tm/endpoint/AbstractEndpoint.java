package ru.t1.nikitushkina.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IServiceLocator;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.request.AbstractUserRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.exception.EndpointException;
import ru.t1.nikitushkina.tm.exception.user.AccessDeniedException;

@Getter
@Setter
public abstract class AbstractEndpoint {

    @NotNull
    @Getter
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request
    ) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable SessionDTO session;
        try {
            session = serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
