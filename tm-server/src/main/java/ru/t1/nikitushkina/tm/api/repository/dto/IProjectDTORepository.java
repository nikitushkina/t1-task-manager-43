package ru.t1.nikitushkina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name,
                      @NotNull String description);

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name);

}
