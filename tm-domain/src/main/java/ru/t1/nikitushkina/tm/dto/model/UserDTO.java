package ru.t1.nikitushkina.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm.tm_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUId = 1;

    @Nullable
    @Column(nullable = false)
    private String login;

    @Nullable
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(length = 100)
    private String email;

    @Nullable
    @Column(name = "first_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "middle_name", length = 100)
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column
    @Nullable
    private Date created = new Date();

    @Column
    @NotNull
    private Boolean locked = false;

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

}
