package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserUnlockResponse extends AbstractResponse {
}
