package ru.t1.nikitushkina.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported.");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command " + command + " not supported.");
    }

}
