package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;

import java.util.List;

public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private final List<TaskDTO> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
